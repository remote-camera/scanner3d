#include "Scanner3D.h"

using namespace cv;

void scanBeaudet2() {
	const Point3i blockSize(500, 500, 500);
	const Point3d dim(100, 100, 100);

	Point dims(4592, 2576);
	double d = 298; // distance to the center of the plate
	double f = 26 / 23.4 * dims.x;

	Matx33d C(f, 0, dims.x * .5,
		0, f, dims.y * .5,
		0, 0, 1);

	Scanner3D scanner(blockSize, dim);
	scanner.loadPhotos(C, d, 36, "photos2/DSC0", ".JPG", 3009);
	auto points = scanner.scan();
	scanner.savePly("beaudet2.ply", points.get());
}

void scanBeaudet3() {
	const Point3i blockSize(1000, 1000, 1000);
	const Point3d dim(250, 200, 250);

	Point dims(4592, 2576);
	double d = 298; // distance to the center of the plate
	double f = 26 / 23.4 * dims.x;

	Matx33d C(f, 0, dims.x * .5,
		0, f, dims.y * .5,
		0, 0, 1);
	
	Scanner3D scanner(blockSize, dim);
	scanner.loadPhotos(C, d, 18, "photos3/DSC0", ".JPG", 3049);
	auto points = scanner.scan();
	scanner.savePly("beaudet4.ply", points.get());
}

void scanBeaudetBresenham() {
	const Point3i blockSize(100, 100, 100);
	const Point3d dim(250, 200, 250);

	Point dims(4592, 2576);
	double d = 298; // distance to the center of the plate
	double f = 26 / 23.4f * dims.x;

	Matx33d C(f, 0, dims.x * .5,
		0, f, dims.y * .5,
		0, 0, 1);

	Scanner3D scanner(blockSize, dim);
	scanner.loadPhotos(C, d, 18, "photos3/DSC0", ".JPG", 3049);
	auto points = scanner.scanBressenham();
	scanner.savePly("beaudet5.ply", points.get());
}

clock_t profile(const string& name, void (*fn)()) {
	clock_t time = clock();
	fn();
	clock_t diff = clock() - time;
	cout << "Processed " << name << " in " << diff * 0.001 / 60 << " minutes." << endl;
	return diff;
}

void compare() {
	int N = 10;
	clock_t t1 = 0, t2 = 0;
	for (int i = 0; i < N; i++) {
		t1 += profile("bresenham", &scanBeaudetBresenham);
		t2 += profile("beaudet3", &scanBeaudet3);
	}
	cout << "T1 = " << (double)t1 / N << endl << "T2 = " << (double)t2 / N << endl;
	cin.ignore();
}

void scanPi() {
	const Point3i blockSize(1200, 1200, 1200);
	const Point3d dim(250, 200, 250);

	Point dims(1616, 1080);
	double d = 298; // distance to the center of the plate
	double f = 26 / 23.4 * dims.x;

	Matx33d C(f, 0, dims.x * .5,
		0, f, dims.y * .5,
		0, 0, 1);

	Scanner3D scanner(blockSize, dim);
	scanner.loadPhotos(C, d, 16, "cactus/picture_", ".jpg", 0);

	auto points = scanner.scan();
	scanner.savePly("cactus.ply", points.get());
}

void backgroundSub() {
	const Point3i blockSize(1000, 1000, 1000);
	const Point3d dim(250, 200, 250);

	Point dims(4896, 3672);
	double d = 298; // distance to the center of the plate
	double f = 26 / 23.4 * dims.x;

	Matx33d C(f, 0, dims.x * .5,
		0, f, dims.y * .5,
		0, 0, 1);

	Scanner3D scanner(blockSize, dim);
	scanner.loadPhotos(C, d, 2, "background/", ".JPG", 1, 1, 1);

	imshow("background", scanner.D.background);
	imshow("fgmodel", scanner.D.fgMask[1]);

	imwrite("fgmask.jpg", scanner.D.fgMask[1]);

	waitKey();
}

int main(int argc, char** arg) {
	//compare();
	//scanBeaudet3();
	//scanBeaudetBresenham();
	//profile("beaudet3", &scanBeaudet3); cin.ignore();
	scanPi();
	//backgroundSub();
	return 0;
}