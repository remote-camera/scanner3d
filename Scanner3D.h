#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <thread>
#include <mutex>

#include "opencv2/opencv.hpp"
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"

using namespace cv;
using namespace std;

class Scanner3D
{
	const double PI = 3.14159265f;

	struct Data {
		vector<Matx34d> projMat;
		vector<Point3i> cameraCenter;
		vector<Mat> images, hsv;
	};

	Data D;

	const Point3i blockSize;
	const Point3d dim;

	Vec3d getCameraCenter(const Matx34d& P) const;
	size_t inline getIndex(int x, int y, int z) const;
	Point3d get3Dcoords(int x, int y, int z) const;
	Point3i vauxel(const Vec3d& p) const;
	Point project(const Point3d& p, const Matx34d& P) const;
	bool Bresenham3D(Point3i p1, const Point3i& p2, const bool* points) const;
	void Bresenham3DScan(Point3i p1, const Point3i & p2, bool* points) const;
	Vec3b getColor(const Point3i& p, const Point3d& pt_3d, const bool* points, int initialGuess = 0) const;
	Matx34d getProjMat(const Matx33d& K, double t, double d) const;
	bool isGreen(const Mat& I, const Point& p) const;
	bool contained(const Point3d& p, int initialGuess = 0) const;

public:
	Scanner3D(const Point3i& blockSize, const Point3d& dim);
	~Scanner3D();
	void loadPhotos(const Matx33d& C, double d, int count, const string& base, const string& ext = ".jpg", int start = 0, int step = 1, int numThreads = 8);
	unique_ptr<bool[]> scan(const int numThreads = 8) const;
	unique_ptr<bool[]> scanBressenham(const int numThreads = 8) const;
	bool savePly(const string& name, const bool* points, int numThreads = 8) const;
};

