#include "Scanner3D.h"



Scanner3D::Scanner3D(const Point3i& blockSize, const Point3d& dim) : blockSize(blockSize), dim(dim)
{
}


Scanner3D::~Scanner3D()
{
}

Vec3d Scanner3D::getCameraCenter(const Matx34d& P) const {
	Matx33d A = P * Matx43d(1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0);
	Vec3d b(P(0, 3), P(1, 3), P(2, 3));

	return -A.inv() * b;
}

Point3i Scanner3D::vauxel(const Vec3d& p) const {
	return Point3i((int) ((p[0] / dim.x + .5) * blockSize.x), (int) ((p[1] / dim.y + .5) * blockSize.y), (int) ((p[2] / dim.z + .5) * blockSize.z));
}

size_t inline Scanner3D::getIndex(int x, int y, int z) const {
	return x + (size_t) blockSize.x * y + (size_t) blockSize.x * blockSize.y * z;
}

Point3d Scanner3D::get3Dcoords(int x, int y, int z) const {
	return Point3d(((double) x / blockSize.x - .5) * dim.x, ((double) y / blockSize.y - .5) * dim.y, ((double) z / blockSize.z - .5) * dim.z);
}

Point Scanner3D::project(const Point3d& p, const Matx34d& P) const {
	Vec4d pt3d_h(p.x, p.y, p.z, 1);
	Vec3d pt_2dh = P * pt3d_h;
	return Point((int) (pt_2dh[0] / pt_2dh[2] + .5), (int) (pt_2dh[1] / pt_2dh[2] + .5));
}

/*
Returns whether there is a point in "points" between p1 and p2.
*/
// TODO : stop after a fixed number of iteration ?
bool Scanner3D::Bresenham3D(Point3i p1, const Point3i& p2, const bool* points) const {
	const size_t N = (size_t) blockSize.x * blockSize.y * blockSize.z;
	size_t index;

	int dx = p2.x - p1.x,
		dy = p2.y - p1.y,
		dz = p2.z - p1.z,
		x_inc = (dx < 0) ? -1 : 1,
		l = abs(dx),
		y_inc = (dy < 0) ? -1 : 1,
		m = abs(dy),
		z_inc = (dz < 0) ? -1 : 1,
		n = abs(dz),
		dx2 = l << 1,
		dy2 = m << 1,
		dz2 = n << 1;

	if ((l >= m) && (l >= n)) {
		int err_1 = dy2 - l,
			err_2 = dz2 - l;
		for (int i = 0; i < l; i++) {
			if (err_1 > 0) {
				p1.y += y_inc;
				err_1 -= dx2;
			}
			if (err_2 > 0) {
				p1.z += z_inc;
				err_2 -= dx2;
			}
			err_1 += dy2;
			err_2 += dz2;
			p1.x += x_inc;

			index = getIndex(p1.x, p1.y, p1.z);
			if (index >= N || index < 0) return true;
			else if (points[index]) return false;
		}
	}
	else if ((m >= l) && (m >= n)) {
		int err_1 = dx2 - m,
			err_2 = dz2 - m;
		for (int i = 0; i < m; i++) {
			if (err_1 > 0) {
				p1.x += x_inc;
				err_1 -= dy2;
			}
			if (err_2 > 0) {
				p1.z += z_inc;
				err_2 -= dy2;
			}
			err_1 += dx2;
			err_2 += dz2;
			p1.y += y_inc;

			index = getIndex(p1.x, p1.y, p1.z);
			if (index >= N || index < 0) return true;
			else if (points[index]) return false;
		}
	}
	else {
		int err_1 = dy2 - n,
			err_2 = dx2 - n;
		for (int i = 0; i < n; i++) {
			if (err_1 > 0) {
				p1.y += y_inc;
				err_1 -= dz2;
			}
			if (err_2 > 0) {
				p1.x += x_inc;
				err_2 -= dz2;
			}
			err_1 += dy2;
			err_2 += dx2;
			p1.z += z_inc;

			index = getIndex(p1.x, p1.y, p1.z);
			if (index >= N || index < 0) return true;
			else if (points[index]) return false;
		}
	}

	index = getIndex(p1.x, p1.y, p1.z);
	if (index >= N || index < 0) return true;
	else return !points[index];
}

void Scanner3D::Bresenham3DScan(Point3i p1, const Point3i& p2, bool* points) const {
	const size_t N = (size_t) blockSize.x * blockSize.y * blockSize.z;
	size_t index;

	int dx = p2.x - p1.x,
		dy = p2.y - p1.y,
		dz = p2.z - p1.z,
		x_inc = (dx < 0) ? -1 : 1,
		l = abs(dx),
		y_inc = (dy < 0) ? -1 : 1,
		m = abs(dy),
		z_inc = (dz < 0) ? -1 : 1,
		n = abs(dz),
		dx2 = l << 1,
		dy2 = m << 1,
		dz2 = n << 1;

	if ((l >= m) && (l >= n)) {
		int err_1 = dy2 - l,
			err_2 = dz2 - l;
		for (int i = 0; i < l; i++) {
			if (err_1 > 0) {
				p1.y += y_inc;
				err_1 -= dx2;
			}
			if (err_2 > 0) {
				p1.z += z_inc;
				err_2 -= dx2;
			}
			err_1 += dy2;
			err_2 += dz2;
			p1.x += x_inc;

			index = getIndex(p1.x, p1.y, p1.z);
			if (index >= N || index < 0) return;
			points[index] = false;
		}
	}
	else if ((m >= l) && (m >= n)) {
		int err_1 = dx2 - m,
			err_2 = dz2 - m;
		for (int i = 0; i < m; i++) {
			if (err_1 > 0) {
				p1.x += x_inc;
				err_1 -= dy2;
			}
			if (err_2 > 0) {
				p1.z += z_inc;
				err_2 -= dy2;
			}
			err_1 += dx2;
			err_2 += dz2;
			p1.y += y_inc;

			index = getIndex(p1.x, p1.y, p1.z);
			if (index >= N || index < 0) return;
			points[index] = false;
		}
	}
	else {
		int err_1 = dy2 - n,
			err_2 = dx2 - n;
		for (int i = 0; i < n; i++) {
			if (err_1 > 0) {
				p1.y += y_inc;
				err_1 -= dz2;
			}
			if (err_2 > 0) {
				p1.x += x_inc;
				err_2 -= dz2;
			}
			err_1 += dy2;
			err_2 += dx2;
			p1.z += z_inc;

			index = getIndex(p1.x, p1.y, p1.z);
			if (index >= N || index < 0) return;
			points[index] = false;
		}
	}

	index = getIndex(p1.x, p1.y, p1.z);
	if (index >= N || index < 0) return;
	points[index] = false;
}

Vec3b Scanner3D::getColor(const Point3i& p, const Point3d& pt_3d, const bool* points, const int initialGuess) const {
	const size_t& N = D.images.size();
	for (size_t i = initialGuess; i < N + initialGuess; i++) {
		if (Bresenham3D(p, D.cameraCenter[i % N], points)) {
			return D.images[i % N].at<Vec3b>(project(pt_3d, D.projMat[i % N]));
		}
	}
	return Vec3b(0, 0, 0);
}

Matx34d Scanner3D::getProjMat(const Matx33d& K, double t, double d) const {
	Vec3d rvec(0, t, 0);
	Matx33d R; Rodrigues(rvec, R);

	Vec3d T = -R* Vec3d(-d*sin(t), 0, d*cos(t));

	Matx34d P;
	hconcat(R, Mat(T), P);
	return K * P;
}

void Scanner3D::loadPhotos(const Matx33d& C, double d, int count, const string& base, const string& ext, int start, int step, int numThreads) {
	vector<Mat> images(count / step);
	vector<Mat> hsv(count / step);
	vector<Matx34d> projMat(count / step);
	vector<Point3i> cameraCenter(count / step);

	vector<thread> threads(numThreads);

	Ptr<BackgroundSubtractor> pMOG2; //MOG2 Background subtractor

	for (int j = 0; j < numThreads; j++) {
		threads[j] = thread([&, j] {

			for (int i = j; i < count / step; i += numThreads) {
				double t = i * step * (360.f / count) / 180.f * PI; // angle of rotation

				projMat[i] = getProjMat(C, t, d);
				cameraCenter[i] = vauxel(getCameraCenter(projMat[i]));
				images[i] = imread(base + to_string(i * step + start) + ext, CV_LOAD_IMAGE_COLOR);
				cvtColor(images[i], hsv[i], CV_BGR2HSV);

			}
		});
	}

	for (thread& t : threads) t.join();

	D.projMat = move(projMat);
	D.cameraCenter = move(cameraCenter);
	D.images = move(images);
	D.hsv = move(hsv);
}

bool Scanner3D::isGreen(const Mat& I, const Point& p) const {
	// pixel is out of the picture, or below half of it
	if (p.x < 0 || p.y < 0 || p.y >= I.rows / 2 || p.x >= I.cols)
		return true;
	const Vec3b& pixel = I.at<Vec3b>(p);
	// pixel is green
	/*if (pixel[0] > 45 && pixel[0] < 75 && pixel[2] > 110)
		return true;*/
	// pixel is cyan
	if (pixel[0] > 75 && pixel[0] < 105 && pixel[2] > 40)
		return true;
	// pixel is red
	if (pixel[0] > 165 && pixel[0] < 15 && pixel[2] > 110)
		return true;
	else
		return false;
}

bool Scanner3D::contained(const Point3d& p, int initialGuess) const {
	const Vec4d pt3d_h(p.x, p.y, p.z, 1);
	const size_t& N = D.images.size();
	for (size_t i = initialGuess; i < initialGuess + N; i++) {
		const Vec3d& pt_2dh = D.projMat[i % N] * pt3d_h;
		if (isGreen(D.hsv[i % N], Point((int) (pt_2dh[0] / pt_2dh[2] + .5), (int) (pt_2dh[1] / pt_2dh[2] + .5))))
			return false;
	}
	return true;
}

unique_ptr<bool[]> Scanner3D::scan(const int numThreads) const {
	// vector<bool> not thread safe !
	const size_t N = (size_t) blockSize.x * blockSize.y * blockSize.z;

	auto pointsPtr = make_unique<bool[]>(N);
	bool* points = pointsPtr.get();

	vector<thread> threads(numThreads);

	for (int i = 0; i < numThreads; i++) {
		threads[i] = thread([&, i] {
			Point3d p;
			p.x = -.5 * dim.x;
			for (int x = 0; x < blockSize.x; x++) {
				p.y = -.5 * dim.y;
				for (int y = 0; y < blockSize.y; y++) {
					p.z = ((double) i / blockSize.z - .5) * dim.z;
					for (int z = i; z < blockSize.z; z += numThreads) {
						//Point3d p(get3Dcoords(x, y, z));
						/*double t = acosf(p.x / sqrtf(p.x * p.x + p.y * p.y));
						if (p.y < 0) t = 2 * PI - t;
						t += PI / 2;
						if (isnan(t)) t = 0;*/
						if (contained(p)) {//(int) (t / PI / 2 * D.images.size()))) {
							points[getIndex(x, y, z)] = true;
						}
						p.z += (double) numThreads / blockSize.z * dim.z;
					}
					p.y += dim.y / blockSize.y;
				}
				p.x += dim.x / blockSize.x;
			}
		});
	}

	for (thread& t : threads) t.join();

	return pointsPtr;
}

// NOT THREAD SAFE ! numThreads must be 1 !!
unique_ptr<bool[]> Scanner3D::scanBressenham(const int numThreads) const {
	const size_t N = (size_t)blockSize.x * blockSize.y * blockSize.z;

	auto pointsPtr = unique_ptr<bool[]>{ new bool[N] };
	bool* points = pointsPtr.get();
	std::fill_n(points, N, true);

	vector<thread> threads(numThreads);

	for (int i = 0; i < numThreads; i++) {
		threads[i] = thread([&, i] {
			for (int j = 0; j < D.hsv.size(); j++) {
				for (int x = 0; x < blockSize.x; x++) {
					for (int y = 0; y < blockSize.y; y++) {
						for (int z = i; z < blockSize.z; z += numThreads) {
							if (points[getIndex(x, y, z)] && isGreen(D.hsv[j], project(get3Dcoords(x, y, z), D.projMat[j]))) {
								points[getIndex(x, y, z)] = false;
								Point3i p1(x, y, z);
								Point3i cam = vauxel(getCameraCenter(D.projMat[j]));
								// remove green points in both directions
								Bresenham3DScan(p1, cam, points);
								Bresenham3DScan(p1, p1 + 10 * (p1 - cam), points);
							}
						}
					}
				}
			}
		});
	}

	for (thread& t : threads) t.join();

	return pointsPtr;
}

// Sauve un maillage triangulaire dans un fichier ply.
// v: sommets (x,y,z), f: faces (indices des sommets), col: couleurs (par sommet) 
bool Scanner3D::savePly(const string& name, const bool* points, int numThreads) const {
	ofstream out(name.c_str());

	if (!out.is_open()) {
		cout << "Cannot save " << name << endl;
		return false;
	}

	const size_t bufsize = 1 << 20;
	unique_ptr<char[]> buf(new char[bufsize]);
	out.rdbuf()->pubsetbuf(buf.get(), bufsize);

	// save space for header
	out.write("                                                                                                                                                                                                           ", 200);

	vector<thread> threads(numThreads);
	mutex l;

	const bool removeBlack = true;
	size_t count = 0;

	for (int i = 0; i < numThreads; i++) {
		threads[i] = thread([&, i] {
			Point3d p;
			p.x = -.5 * dim.x;
			for (int x = 0; x < blockSize.x; x++) {
				p.y = -.5 * dim.y;
				for (int y = 0; y < blockSize.y; y++) {
					p.z = ((double)i / blockSize.z - .5) * dim.z;
					for (int z = i; z < blockSize.z; z += numThreads) {
						if (points[getIndex(x, y, z)]) {
							double t = acos(p.x / sqrt(p.x * p.x + p.z * p.z));
							if (p.y < 0) t = 2 * PI - t;
							if (isnan(t)) t = 0;
							Vec3b color = getColor(Point3i(x, y, z), p, points, (int) (.5 + t / PI / 2 * D.images.size()) % D.images.size());
							if (!removeBlack || color != Vec3b(0, 0, 0)) {
								l.lock();
								count++;
								out << p.x << " " << p.y << " " << p.z << " " << (int)color[2] << " " << (int)color[1] << " " << (int)color[0] << '\n';
								l.unlock();
							}
						}
						p.z += (double) numThreads / blockSize.z * dim.z;
					}
					p.y += dim.y / blockSize.y;
				}
				p.x += dim.x / blockSize.x;
			}
		});
	}

	for (thread& t : threads) t.join();

	out.seekp(0);

	out << "ply" << '\n'
		<< "format ascii 1.0" << '\n'
		<< "element vertex " << count << '\n'
		<< "property float x" << '\n'
		<< "property float y" << '\n'
		<< "property float z" << '\n'
		<< "property uchar red" << '\n'
		<< "property uchar green" << '\n'
		<< "property uchar blue" << '\n'
		<< "end_header" << '\n';

	out.close();
	return true;
}