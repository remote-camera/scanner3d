### Modal Image : Scanner 3D

Notre projet est composé de deux éléments : un fichier **main** permettant d'éxécuter l'algorithme et une classe **Scanner3D** qui réunit l'ensemble des fonctions de l'algorithme.
Parmi elles, on note trois fonctions publiques, _loadPhotos_, _scan_ et _savePly_ permettant respectivement d'initialiser la classe,
de scanner l'objet et de sauvegarder le modèle dans un fichier ".ply".
Une dernière fonction _scanBresenham_ était une tentative d'amélioration de l'algorithme qui est finalement moins performante car elle demande d'être éxécutée sur un seul processeur.

Dans le main, il est possible de modifier divers parmètres tels que blockSize afin de tester les performances de l'algorithme.
Les différentes fonctions _scanBeaudetI_, I allant de 1 à 3 permettent de scanner différentes modèles. On retrouve le résultat dans un fichier beaudetI.ply dans le dossier bin.
Les fonctions _compare_ et _profile_ permettent de tester les performances.